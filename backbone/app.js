Backbone.pubSub = _.extend({}, Backbone.Events);

var detailsTemplate = _.template(
    "<p><%- PersonName %></p>" +
    "<p><%- PersonDetails %></p>");

var DetailsView = Backbone.View.extend({
    el: '#details',

    initialize: function() {
        _.bindAll(this, "showDetails");
        Backbone.pubSub.on("showDetails", this.showDetails, this);
    },

    showDetails: function(person) {
        this.model = person;
        this.render();
    },

    render: function() {
        var html=detailsTemplate(this.model);
        this.$el.html(html);
        return this;
    }
});

var rowTemplate=_.template("<tr rowId=<%= PersonID %>>"+
     "<td class='PersonID'><%= PersonID %></td>"+
     "<td class='PersonName'><%= PersonName %></td>"+
     "</tr>");

/** View representing a table */
var TableView = Backbone.View.extend({
    el: '#data-rows',
    collection: new Backbone.Collection(),
    initialize : function() {
        _.bindAll(this,'render','renderOne');
        this.collection.url = "/persons.json";
        _this = this;
        this.collection.fetch({
            success: _this.render
        });
    },
    render: function() {
        this.collection.each(this.renderOne);
        return this;
    },
    renderOne : function(model) {
        var row=new RowView({model:model});
        this.$el.append(row.render().$el);
        return this;
    }
});

/** View representing a row of that table */
var RowView = Backbone.View.extend({
    events: {
        "click": function() {
            $('.active').removeClass('active');
            $('tr[rowId='+this.model.get("PersonID")+']').addClass('active');
            Backbone.pubSub.trigger("showDetails", this.model.toJSON());
        }
    },

    render: function() {
        var html=rowTemplate(this.model.toJSON());
        this.setElement( $(html) );
        return this;
    }
});

var PersonModel = Backbone.Model.extend({
    defaults: {
        active: false
    }
});

/** Collection of models to draw */
var tableView = new TableView();
var detailsView = new DetailsView();
