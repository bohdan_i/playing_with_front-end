import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import { Grid, Row, Col } from 'react-bootstrap';
import 'react-bootstrap-table';

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            records: [],
            selected: undefined
        }

        this.showDetails = this.showDetails.bind(this);
        this.updateRecords = this.updateRecords.bind(this);
    }

    showDetails(id) {
        var newState = this.state;
        newState.selected = id;
        this.setState(newState);
    }

    updateRecords(records) {
      var newState = this.state;
      newState.records = records;   
      this.setState(newState);
    }

    componentWillMount() {
      var _this = this;
      this.serverRequest = 
        axios
          .get("persons.json")
          .then(function(result) {
            _this.updateRecords(result.data);
          })
    }

    componentWillUnmount() {
      this.serverRequest.abort();
    }

    render() {
        return (
            <Grid>
              <Col md={6} ><Details obj={this.state.selected} /></Col>
              <Col md={6} ><RowClickTable data={this.state.records} onRowClick={ this.showDetails } /></Col>
            </Grid>
        );
    }
}

class Details extends React.Component {
    render() {
        let infoBlock = null;
        if (typeof(this.props.obj) == 'undefined') {
            infoBlock = <p>Here will be detailed info</p>;
        } else {
            infoBlock = 
            <div>
              <p>{this.props.obj.name}</p>
              <p>{this.props.obj.descr}</p>
            </div>
        }
        return (
            <div>
              <h2>Details</h2>
              {infoBlock}
            </div>
        );
    }
}

class RowClickTable extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
        selected: []
      };
    }

  render() {
    const onRowSelect = ({ id }, isSelected) => {
      if (isSelected) {
        this.setState({
          selected: [  id ]
        });
      } else {
        this.setState({ selected: 0 });
      }
      return false;  //important
    };

    const selectRowProp = {
      mode: 'radio',
      hideSelectColumn: true,
      clickToSelect: true,
      onSelect: onRowSelect,  // manage the selection state
      bgColor: 'grey',
      selected: this.state.selected   // set selected as a state data
    };

    const options = {
      expandRowBgColor: 'rgb(242, 255, 163)',
      onRowClick: this.props.onRowClick
    };

    return (
      <BootstrapTable 
        options={ options }
        selectRow={ selectRowProp }
        ref='table1' data={ this.props.data } >
        <TableHeaderColumn dataField='id' isKey>Person ID</TableHeaderColumn>
        <TableHeaderColumn dataField='name'>Person Name</TableHeaderColumn>
      </BootstrapTable>
    );
  }

}

export default App;