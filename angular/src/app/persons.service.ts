import { Injectable } from '@angular/core'; 
import { Http , Response } from '@angular/http'; 
import { Observable } from 'rxjs/Observable'; 
import 'rxjs/add/operator/map'; 
import 'rxjs/add/operator/do'; 
import { IPerson } from './person';  

@Injectable() 
export class PersonService { 
   private _personurl='app/persons.json'; 
   constructor(private _http: Http){} 
   getpersons(): Observable<IPerson[]> { 
      return this._http.get(this._personurl) 
      .map((response: Response) => <IPerson[]> response.json()) 
      .do(data => console.log(JSON.stringify(data))); 
   } 
} 