import { Component } from '@angular/core';
import { IPerson } from './person'; 
import { PersonService } from './persons.service'; 

import { Http , Response } from '@angular/http'; 
import { Observable } from 'rxjs/Observable'; 
import 'rxjs/add/operator/map';  

@Component({
  selector: 'my-app',
  templateUrl: 'app/app.component.html',
  providers: [PersonService]
})
export class AppComponent  {
  ipersons: IPerson[];
  selectedPerson: IPerson;
  name = 'Angular';
  selectedRow : Number;
  setClickedRow : Function;

  constructor(private _person: PersonService){
    this.selectedPerson = null;
    this.setClickedRow = function(i: Number, person: IPerson){
      this.selectedRow = i;
      this.selectedPerson = person;
    }
  } 
  
  ngOnInit() : void { 
     this._person.getpersons() 
     .subscribe(ipersons => this.ipersons = ipersons); 
  } 
}
