export interface IPerson { 
   PersonID: number; 
   PersonName: string; 
   PersonDetails: string;
}